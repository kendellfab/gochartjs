package gochartjs

import (
	"hash/fnv"
)

type Chart struct {
	Data         ChartData             `json:"data"`
	Options      ChartOptions          `json:"options"`
	usedColorSet map[string]EmptyColor `json:"-"`
}

func NewChart(chartLabel string) *Chart {
	return &Chart{Data: ChartData{},
		Options: ChartOptions{AspectRatio: 2,
			Responsive:          true,
			MaintainAspectRatio: false,
			Title: ChartTitle{Display: true,
				Text: chartLabel,
			},
			Legend: ChartLegend{Position: "right"},
		},
		usedColorSet: make(map[string]EmptyColor),
	}
}

func (c *Chart) AddLabels(labels []string) *Chart {
	c.Data.Labels = labels
	return c
}

func (c *Chart) AddDatum(d ChartDatum) {
	if d.BorderColor == "" && d.BackgroundColor != "" {
		d.BorderColor = d.BackgroundColor
	} else if d.BackgroundColor == "" && d.BorderColor != "" {
		d.BackgroundColor = d.BorderColor
	} else {
		c := c.selectColor(d.Label)
		d.BorderColor = c
		d.BackgroundColor = c
	}
	c.Data.DataSets = append(c.Data.DataSets, d)
}

func (c *Chart) AddXAxes(label string, autoSkip bool) *Chart {
	c.Options.Scales.XAxes = append(c.Options.Scales.XAxes, ChartAxes{Display: true, ChartScaleLabel: ChartScaleLabel{Display: true, LabelString: label}, Tick: ChartTick{AutoSkip: autoSkip}})

	return c
}

func (c *Chart) AddYAxes(label string, autoSkip bool) *Chart {
	c.Options.Scales.YAxes = append(c.Options.Scales.YAxes, ChartAxes{Display: true, ChartScaleLabel: ChartScaleLabel{Display: true, LabelString: label}, Tick: ChartTick{AutoSkip: autoSkip}})

	return c
}

func (c *Chart) selectColor(label string) string {
	h := fnv.New32a()
	h.Write([]byte(label))
	hashVal := int(h.Sum32())

	idx := hashVal % len(colorCodes)
	col := colorCodes[idx]

	for {
		if _, ok := c.usedColorSet[col]; !ok {
			c.usedColorSet[col] = EmptyColor{}
			return col
		}

		idx++
		col = colorCodes[idx]
	}
}
