package gochartjs

type ChartData struct {
	Labels   []string     `json:"labels"`
	DataSets []ChartDatum `json:"datasets"`
}

type ChartDatum struct {
	Label           string        `json:"label"`
	BackgroundColor string        `json:"backgroundColor,omitempty"`
	BorderColor     string        `json:"borderColor,omitempty"`
	Data            []interface{} `json:"data"`
	Fill            bool          `json:"fill"`
}