package gochartjs

type ChartOptions struct {
	AspectRatio         float32     `json:"aspectRatio,omitempty"`
	Responsive          bool        `json:"responsive"`
	MaintainAspectRatio bool        `json:"maintainAspectRatio"`
	Title               ChartTitle  `json:"title"`
	Scales              ChartScale  `json:"scales"`
	Legend              ChartLegend `json:"legend"`
}

type ChartTitle struct {
	Display bool   `json:"display"`
	Text    string `json:"text"`
}

type ChartScale struct {
	XAxes []ChartAxes `json:"xAxes,omitempty"`
	YAxes []ChartAxes `json:"yAxes,omitempty"`
}

type ChartAxes struct {
	Display         bool            `json:"display,omitempty"`
	ChartScaleLabel ChartScaleLabel `json:"scaleLabel,omitempty"`
	Tick            ChartTick       `json:"ticks"`
}

type ChartTick struct {
	AutoSkip bool `json:"autoSkip"`
}

type ChartScaleLabel struct {
	Display     bool   `json:"display,omitempty"`
	LabelString string `json:"labelString"`
}

type ChartLegend struct {
	Display  bool   `json:"display,omitempty"`
	Position string `json:"position,omitempty"`
}
